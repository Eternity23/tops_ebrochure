import React, { useRef, useState } from "react";
import ReactDOM from "react-dom";
import $ from "jquery";
import "turn.js";
import VideoJS from "./VideoJS";
import { Pagination } from 'antd';
import { LeftOutlined, RightOutlined, DownloadOutlined, LinkOutlined, ZoomInOutlined } from '@ant-design/icons';
import { Slider } from 'antd';

const BOOK_WIDTH = 739;
const BOOK_HEIGHT = 1024;
const DOMAIN_PATH = "/temp/ebrochure";

class Turn extends React.Component {
    static defaultProps = {
        style: {},
        className: "",
        options: {}
    };

    componentDidMount() {
        if (this.el) {
            $(this.el).turn(Object.assign({}, this.props.options));
            $(this.el).on("turn", (event) => {
                console.log("turned " + $(this.el).turn("page"))
                if (this.props.onTurned) {
                    this.props.onTurned($(this.el).turn("page"));
                }
            })
        }
        document.addEventListener("keydown", this.handleKeyDown, false);
    }

    componentWillUnmount() {
        if (this.el) {
            // try{

            //     $(this.el)
            //     .turn("destroy")
            //     .remove();
            // }
            // catch(err){

            // }
        }
        document.removeEventListener("keydown", this.handleKeyDown, false);
    }


    previousPage = () => {
        const currentPage = $(this.el).turn("page");
        const viewPages = $(this.el).turn("view");
        console.log(viewPages);
        var newPage = currentPage - 1;
        if (viewPages.length == 2) {
            if (viewPages.indexOf(currentPage) == 1) {
                newPage -= 1;
            }
        }
        if (currentPage > 1) {
            this.gotoPage(newPage);
        }
        else {
            newPage = 1;
        }
        return newPage;
    }


    nextPage = () => {
        const currentPage = $(this.el).turn("page");
        const totalPages = $(this.el).turn("pages");
        const viewPages = $(this.el).turn("view");

        console.log(viewPages);
        var newPage = currentPage + 1;

        if (viewPages.length == 2) {
            if (viewPages.indexOf(currentPage) == 0) {
                newPage += 1;
            }
        }
        if (currentPage < totalPages) {
            this.gotoPage(newPage);
        }
        else {
            newPage = totalPages;
        }
        return newPage;
    }

    zoom = (level) => {
        $(this.el).turn("zoom", level);
    }
    gotoPage = (page) => {
        $(this.el).turn("page", page);
    }

    handleKeyDown = event => {
        if (event.keyCode === 37) {
            $(this.el).turn("previous");
        }
        if (event.keyCode === 39) {
            $(this.el).turn("next");
        }
    };

    render() {
        return (
            <div
                className={this.props.className}
                style={Object.assign({}, this.props.style)}
                ref={el => (this.el = el)}
            >
                {this.props.children}
            </div>
        );
    }
}

const options = {
    width: 800,
    height: 600,
    autoCenter: false,
    display: "double",
    acceleration: true,
    elevation: 50,
    gradients: !$.isTouch,
    when: {
        turned: function (e, page) {
            console.log("Current view: ", $(this).turn("view"));
        }
    }
};

/////////Hit Area Contents//////////////////////////
const pageComponents = [
    {
        id: "01",
        name: "Cocoa Dutch",
        page: 1,
        action: "link",
        hitArea: [15, 786, 413, 184], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/th/cocoa-dutch-cocoa-powder-200g-8850639004595?queryID=94cc83377f198c23ddb45fe613444435&queryIndex=tops_prod_default_products&objectID=42343-tops_sa_432_th&position=5&query=dutch&from=algolia_search_plp"
    },
    {
        id: "08",
        name: "Imperial Pancake Flour 400g.",
        page: 1,
        action: "link",
        hitArea: [220, 293, 208, 239], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/en/imperial-pancake-flour-400g-8850332192117"
    },
    {
        id: "09",
        name: "Tops Cashew Nuts 300g.",
        page: 1,
        action: "link",
        hitArea: [17, 535, 208, 239], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/en/tops-cashew-nuts-300g-8853474010813"
    },
    {
        id: "09",
        name: "Tops Cashew Nuts 300g.",
        page: 1,
        action: "link",
        hitArea: [220, 535, 208, 239], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/th/diamond-baking-paper-size-30cm-x8metre-0010900740104"
    },
    {
        id: "02",
        name: "Tops up your love",
        page: 1,
        action: "link",
        hitArea: [445, 183, 279, 786], //[x,y,width,height] เป็น pixel
        value: "https://www.facebook.com/story.php?story_fbid=1263768204024784&id=631745327227078"
    },
    {
        id: "03",
        name: "Magnolia Lactose Free",
        page: 1,
        action: "link",
        hitArea: [17, 293, 208, 239], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/th/magnolia-plus-pasteurized-lactose-free-milk-plain-946ml-8858705608708?queryID=6030551593f952e80c06e6289a074324&queryIndex=tops_prod_default_products&objectID=74845-tops_sa_432_th&position=2&query=magnolia%20lactose&from=algolia_search_plp"
    },
    {
        id: "04",
        name: "Wogan Mandarin",
        page: 2,
        action: "link",
        hitArea: [15, 230, 280, 375], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/en/wogan-mandarin-0214145000003"
    },
    {
        id: "05",
        name: "S&P Vienna Sausage 160g.",
        page: 2,
        action: "link",
        hitArea: [15, 630, 320, 280], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/en/s-p-vienna-sausage-160g-8850201871310"
    },
    {
        id: "06",
        name: "My Choice Extra Virgin Olive Oil Cold Extraction 500ml.",
        page: 2,
        action: "link",
        hitArea: [370, 630, 350, 280], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/en/my-choice-extra-virgin-olive-oil-cold-extraction-500ml-8853474047741"
    },
    {
        id: "10",
        name: "butter-cream",
        page: 2,
        action: "link",
        hitArea: [320, 230, 400, 375], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/th/fresh-food-bakery/butter-cream"
    },
    /*{
        id: "07",
        name: "Tops Video",
        page: 3,
        action: "video",
        hitArea: [5, 330, 720, 600], //[x,y,width,height] เป็น pixel
        value: "https://www.youtube.com/embed/46EoAlSrBtw"
    },*/
    /*{
        id: "07",
        name: "Tops Video",
        page: 3,
        action: "video",
        hitArea: [5, 130, 720, 600], //[x,y,width,height] เป็น pixel
        value: "https://www.youtube.com/embed/46EoAlSrBtw"
    },*/
    /*{
        id: "11",
        name: "powder-health-tonic",
        page: 3,
        action: "link",
        hitArea: [15, 390, 700, 240], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/en/beverages/powder-health-tonic"
    },*/
    {
        id: "12",
        name: "baking-and-dessert-ingredients",
        page: 3,
        action: "link",
        hitArea: [15, 50, 380, 260], //[x,y,width,height] เป็น pixel
        value: "https://www.tops.co.th/en/pantry-and-ingredients/baking-and-dessert-ingredients"
    },
    {
        id: "07",
        name: "Tops Video",
        page: 3,
        action: "video",
        hitArea: [5, 330, 720, 600], //[x,y,width,height] เป็น pixel
        // value: "/temp/ebrochure/videos/mp4/Rare Item ใน ​TOPS​ MARKET​.mp4"
        value: "http://uatdg-directory.cpn.co.th/temp/ebrochure/videos/mp4/Rare Item ใน ​TOPS​ MARKET​.mp4"
    },
];

// const pages = [
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_1.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_2.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_3.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_4.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_5.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_6.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_7.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_8.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_9.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_10.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_11.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_12.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_13.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_14.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_15.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_16.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_17.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_18.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_19.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_20.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_21.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_22.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_23.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_24.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_25.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_26.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_27.jpg",
//     "/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_28.jpg",
// ];
const book1 = [
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_1.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_2.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_3.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_4.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_5.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_6.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_7.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_8.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_9.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_10.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_11.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_12.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_13.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_14.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_15.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_16.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_17.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_18.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_19.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_20.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_21.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_22.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_23.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_24.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_25.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_26.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_27.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook1024_28.jpg",
];
const book2 = [
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-01.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-02.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-03.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-04.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-05.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-06.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-07.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-08.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-09.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-10.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-11.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-12.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-13.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-14.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-15.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-16.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-17.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-18.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-19.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-20.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-21.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-22.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-23.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-24.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-25.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-26.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-27.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-28.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-29.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-30.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-31.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-32.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-33.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-34.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-35.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-36.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-37.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-38.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-39.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-40.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-41.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-42.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-43.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-44.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-45.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-46.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-47.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-48.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-49.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-50.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-51.jpg",
    "http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17-52.jpg"
];

const DemoBook = () => {
    const turnRef = useRef(null);
    const videoPlayerRef = useRef(null);
    const [currentPage, setCurrentPage] = useState(1);

    return (
        <div style={{ width: "100%", height: "100%", backgroundColor: '#4E5565' }}>
            <div style={{ zIndex: 50, width: "100%", height: "100px", display: "flex", alignItems: "center", justifyContent: "center" }}>
                {/* <a href="http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book1/TSS wk08-09 ebook.pdf" style={{ */}
                <a href="http://uatdg-directory.cpn.co.th/temp/ebrochure/pdf/books/book2/E book TMK wk17.pdf" style={{
                    border: "0px solid red"
                }} download="TSS wk08-09 ebook" target="_blank" ><DownloadOutlined style={{ zIndex: 50, fontSize: '25px', color: 'white', margin: '10px', filter: "drop-shadow(1px 1px 0px rgb(0 0 0 / 0.7))" }} onClick={() => {
                }} /></a>
                <ZoomInOutlined style={{ zIndex: 50, fontSize: '25px', color: 'grey', margin: '10px' }} />
                {/* <ZoomInOutlined  style={{zIndex:50, fontSize:'25px', color:'grey',margin:'10px',filter: "drop-shadow(1px 1px 0px rgb(0 0 0 / 0.7))"}} disabled onClick={()=>{
            if(turnRef && turnRef.current)
            {
                turnRef.current.zoom(2);
                // setCurrentPage(page);
            }
        }}/> */}
                {/* <LinkOutlined style={{fontSize:'25px', color:'grey',margin:'10px'}}/> */}
            </div>
            <div style={{ width: "100%", height: "calc(100% - 200px)", display: "flex", flexDirection: "collumn", position: "relative", alignItems: "center", justifyContent: "center" }}>
                <div stly={{ flex: 1, zIndex: 50 }}>

                    <LeftOutlined style={{ float: "right", margin: "20px", fontSize: '40px', color: 'white' }} onClick={() => {
                        if (turnRef && turnRef.current) {
                            turnRef.current.previousPage();
                            // setCurrentPage(page);
                        }
                    }} />
                </div>
                <div stly={{ zIndex: 10, flex: 1, position: "relative" }}>
                    <Turn onTurned={(page) => {
                        console.log("turned page " + page)
                        console.log("videoPlayerRef " + videoPlayerRef.current)
                        setCurrentPage(page);
                        if (videoPlayerRef.current) {
                            videoPlayerRef.current.play();
                        }

                    }} ref={turnRef} options={options} className="magazine" style={(currentPage === 28) ? { marginRight: "-400px" } : { marginLeft: (currentPage === 0 || currentPage === 1) ? "-400px" : "0px" }}>
                        {book2.map((page, index) => {
                            const pageNum = index + 1;

                            const hitAreas = pageComponents.filter((element) => {
                                return (element.page === pageNum)
                            }).map((cData) => {
                                const xPercent = 100 * cData.hitArea[0] / BOOK_WIDTH
                                const yPercent = 100 * cData.hitArea[1] / BOOK_HEIGHT
                                const widthPercent = 100 * cData.hitArea[2] / BOOK_WIDTH
                                const heightPercent = 100 * cData.hitArea[3] / BOOK_HEIGHT
                                switch (cData.action) {
                                    case "link":
                                        return <a href={cData.value} target="_blank" style={{ position: "absolute", left: `${xPercent}%`, top: `${yPercent}%`, width: `${widthPercent}%`, height: `${heightPercent}%`, backgroundColor: "rgba(0,0,0,0)" }}></a>
                                        break;
                                    case "video":

                                        const videoJsOptions = { // lookup the options in the docs for more options
                                            autoplay: true,
                                            muted: true,
                                            loop: true,
                                            fill: true,
                                            controls: true,
                                            responsive: false,
                                            sources: [{
                                                src: cData.value,
                                                type: 'video/mp4'
                                            }]
                                        }
                                        const handlePlayerReady = (player) => {
                                            videoPlayerRef.current = player;
                                            player.on('ready', () => {
                                                console.log('player is ready');
                                            });
                                            // you can handle player events here
                                            player.on('waiting', () => {
                                                console.log('player is waiting');
                                            });

                                            player.on('dispose', () => {
                                                console.log('player will dispose');
                                            });
                                            player.on('suspended', () => {
                                                console.log('player will suspended');
                                            });
                                            player.on('ended', () => {
                                                console.log('player will ended');
                                            });
                                        };
                                        return <VideoJS ref={videoPlayerRef} style={{ position: "absolute", objectFit: "cover", left: `${xPercent}%`, top: `${yPercent}%`, width: `${widthPercent}%`, height: `${heightPercent}%` }} options={videoJsOptions} onReady={handlePlayerReady} />
                                        break;
                                }
                            })

                            return (<div key={index} className="page" >

                                <div style={{ position: "relative", width: "100%", height: "100%" }}>

                                    <div className="gradient" style={{ width: "100%", height: "100%" }}></div>
                                    <img style={{ position: "absolute" }} src={page} alt="" />
                                    {hitAreas}
                                </div>
                            </div>
                            )
                        })}
                    </Turn>
                </div>
                <div stly={{ zIndex: 50, flex: 1 }}>
                    <RightOutlined style={{ float: "left", fontSize: '40px', color: 'white', margin: "20px" }} onClick={() => {
                        if (turnRef && turnRef.current) {
                            turnRef.current.nextPage();
                            // setCurrentPage(page);
                        }
                    }} />
                </div>
                {/* <Pagination style={{margin:"10px"}} current={currentPage} onChange={(page)=>{
            console.log(page)
            if(turnRef && turnRef.current)
            {
                turnRef.current.gotoPage(page);
                setCurrentPage(page);
            }
            }} simple pageSize={1} total={28}></Pagination> */}
            </div>
            <div style={{ width: "100%", height: "100px", display: "flex", flexDirection: "collumn", position: "absolute", alignItems: "center", justifyContent: "center" }}>
                <Slider style={{ paddingTop: "10px", width: "40%" }} defaultValue={1} tooltipVisible={false} min={1} max={28} value={currentPage} onChange={(page) => {
                    if (turnRef && turnRef.current) {
                        turnRef.current.gotoPage(page);
                        setCurrentPage(page);
                    }
                }}></Slider>
            </div>
        </div>
    );
};

export default DemoBook;
